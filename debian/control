Source: repomaker
Section: devel
Priority: optional
Maintainer: Hans-Christoph Steiner <hans@eds.org>
Uploaders: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Build-Depends: debhelper (>= 9.0~),
               bash-completion,
               dh-exec,
               dh-python,
               fdroidserver,
               python3,
               python3-django,
               python3-django-allauth,
               python3-django-background-tasks,
               python3-django-hvad,
               python3-django-js-reverse,
               python3-django-sass-processor,
               python3-magic,
               python3-setuptools,
Standards-Version: 4.1.0
Testsuite: autopkgtest-pkg-python
Homepage: https://f-droid.org
Vcs-Git: https://salsa.debian.org/debian/repomaker.git
Vcs-Browser: https://salsa.debian.org/debian/repomaker

Package: repomaker
Architecture: all
Depends: ${python3:Depends},
         aapt,
         fdroidserver,
         fonts-roboto-fontface,
         fonts-materialdesignicons-webfont,
         ${misc:Depends}
Recommends: apksigner,
            git,
            s3cmd,
Description: Web app for creating and managing F-Droid repos
 F-Droid.org is just a repo out of hundreds of repos created by
 individuals all around the globe. With the tools of F-Droid, everyone
 can create their own repo. So whether you are a musician who wants to
 publish their music or a developer who wants to serve nightly builds
 of their app, you are free to create your own repo and share it with
 other people independently of F-Droid.org.
 .
 In the past, creating a repo has been difficult because you had to
 have knowledge on the command line, needed to edit text files to edit
 your packages’ store details and had to paste screenshots in a
 special system of directories to have them served well inside the
 F-Droid app.
 .
 This all got easier now: with Repomaker, you are able to create your
 own repo and do not need to have any special knowledge to do so. More
 information on how to install Repomaker coming soon!
