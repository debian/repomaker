��          T      �       �      �      �      �      �   *   �       %  �  F  (   �            $   ,  -   Q  +                                           %s app added %s apps added Add Added Try to drag and drop again! Uploading %s file... Uploading %s files... You can only upload images here. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-07-14 06:50+0000
Last-Translator: Claus Rüdinger <Mail-an-CR@web.de>
Language-Team: German <https://hosted.weblate.org/projects/f-droid/repomaker-javascript/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.16-dev
 %s App hinzugefügt %s Apps hinzugefügt Hinzufügen Hinzugefügt Ziehen und ablegen erneut versuchen! Lade %s Datei hoch... Lade %s Dateien hoch... Hier können nur Bilder hochgeladen werden. 