��    d      <  �   \      �     �  
   �     �     �  
   �  	   �     �     �  C   �  E   	     I	     O	     T	     W	     e	     n	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     
     $
  	   1
     ;
     S
     g
     s
     y
     �
     �
  L   �
     �
     �
     �
               !     '     .     7     <     E     J  *   P     {     �  3   �  3   �     �     �  
   	                -     4     B     H     N     l          �  )   �     �  ,   �     
      )  /   J  6   z     �  !   �  E   �     4     8     D     Z  	   r     |     �     �     �  =   �            '   /  I   W     �  	   �     �     �     �  	   �  �  �  
   �  	   �     �     �     �     �     �     �  A   �  F        Z     `     f     i  	   w     �     �     �     �     �  	   �     �     �     �     �  	          %        =     F     W     e     �     �     �     �     �     �  X   �     9     B     O     b     p     w     }     �     �  
   �     �     �  .   �     �     �  7   �  1   (  
   Z     e     y     �     �     �     �     �  	   �      �     �            +        C  (   c  "   �  $   �  *   �  :   �     :  )   V  M   �     �     �     �     �                ,     L     l  G   r     �     �  +   �  Z        x  
   �     �     �     �  	   �     "   _   ;   F   ]          @              P       6   2   B          9   c       T   W   :      '           A   I          =   S   Y                  >      
   -   `                 0   \   N   3   (   ?             Z   L   #   +              V   X               [       !       E   %   $       7      )      d   8      /               a              D                 <      5           1   H   J       &          b   *                    G   	      U   ^      Q   M   O      .   K   4       R   ,             C    10'' Tablet 7'' Tablet APK Add Add a repo Add files Added All Are you sure you want to delete the repo <strong>%(repo)s</strong>? Are you sure you want to delete your app <strong>%(object)s</strong>? Audio Book By By %(author)s Category Category (%(count)s) Change E-mail Choose Category Confirm Create Create Repo Delete App Delete Repo Describe the repo Description Document Done Easy, free, requires an account Edit Edit %(app)s Edit Repo Enter a valid hostname. Enter a valid path. Enter email Error Forgot Password Git Hello, %(user)s If you just added a repo, wait for the apps to download and try again later. Image Info Invalid APK signature Invalid APK. Language Login Logout My Repos Name New Repo Next Other Other regions are currently not supported. Path Phone Please don't add one of your own repositories here. Please only proceed if you know what you are doing! Previous Released %(date)s Remove App Remove Repo Save Changes Search See you later Setup Share Short Summary (40 characters) Source (%(count)s) Spread the love. TV There are no apps available in the repos. There are no apps in this repo. This APK already exists in the current repo. This URL must end with '.git'. This URL must start with 'git@'. This app does already exist in your repository. This app does not have any working versions available. This app has no APK files. This file is not an update for %s This file is of a different type than the other versions of this app. URL US Standard Unsupported File Type Update %(storage_name)s User Name Username Version %(version)s (%(code)s) Version %(version)s (%(date)s) Video We have sent you an email with a link to reset your password. Wearable Yes, I want to delete the repo Your search did not return any results. Your security is important to us. Please confirm that you want to logout. add from gallery copy link logout or save view repo Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-07-14 23:57+0000
Last-Translator: Roberto Albano De Rosa <robertoalbano@protonmail.com>
Language-Team: Italian <https://hosted.weblate.org/projects/f-droid/repomaker/it/>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.16-dev
 10" Tablet 7" Tablet APK Aggiungi Aggiungi un repo Aggiungi file Aggiunto Tutto Sei sicuro di voler cancellare il repo <strong>%(repo)s</strong>? Sei sicuro di voler cancellare la tua app <strong>%(object)s</strong>? Audio Libro Di Di %(author)s Categoria Categoria (%(count)s) Cambia E-mail Scegli categoria Conferma Crea Crea repo Cancella App Cancella repo Descrivi il repo Descrizione Documento Finito Semplice, gratis, richiede un account Modifica Modifica %(app)s Modifica repo Inserisci un hostname valido. Inserisci un percorso valido. Inserisci email Errore Password dimenticata Git Ciao, %(user)s Se hai appena aggiunto un repo attendi che le app vengano scaricate e prova ancora dopo. Immagine Informazioni Firma APK invalida APK invalido. Lingua Login Logout I miei repo Nome Nuovo repo Prossimo Altro Altre regioni non sono attualmente supportate. Percorso Telefono Per favore, non aggiungere uno dei tuoi repository qui. Per favore procedi solo se sai cosa stai facendo! Precedente Rilasciato %(date)s Rimuovi App Rimuovi repo Salva cambiamenti Cerca Ci vediamo dopo Imposta Condividi Piccolo Riassunto (40 caratteri) Sorgente (%(count)s) Diffondi l'amore. TV Non ci sono app disponibili in questi repo. Non ci sono app in questo repo. Questo APK esiste già nel repo attuale. Questo URL deve finire con '.git'. Questo URL deve iniziare con 'git@'. Questa app esiste già nel tuo repository. Questa app non ha alcuna versione funzionante disponibile. Questa app non ha file APK. Questo file non è un aggiornamento di %s Questo file è di un tipo diverso rispetto alle altre versioni di questa app. URL Standard US Tipo di file non supportato Aggiorna %(storage_name)s Nome utente Nome utente Versione %(version)s (%(code)s) Versione %(version)s (%(date)s) Video Ti abbiamo inviato una mail con il link per recuperare la tua password. Dispositivo indossabile Sì, voglio cancellare il repo La tua ricerca non ha restituito risultati. La tua sicurezza è importante per noi. Per favore conferma che vuoi effettuare il logout. aggiungi dalla galleria copia link logout o salva vedi repo 